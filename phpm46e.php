<!DOCTYPE html>
<html>
<head>
    <title> PHP Quiz easy </title>
    <meta name="viewport" content="width=device-width, initial-scale=1.0">
    <meta charset="utf-8">

    <!-- HTML5 shim and Respond.js IE8 support of HTML5 elements and media queries -->
    <!--[if lt IE 9]>
    <script src="http://cdnjs.cloudflare.com/ajax/libs/html5shiv/3.6.2/html5shiv.js"></script>
    <script src="http://cdnjs.cloudflare.com/ajax/libs/respond.js/1.2.0/respond.js"></script>
    <![endif]-->

    <!-- Load Bootstrap JavaScript components -->
    <script src="http://code.jquery.com/jquery-2.1.1.min.js"></script>
    <!-- Latest compiled and minified CSS -->
    <link rel="stylesheet" href="https://maxcdn.bootstrapcdn.com/bootstrap/3.2.0/css/bootstrap.min.css">
    <!-- Latest compiled and minified JavaScript -->
    <script src="https://maxcdn.bootstrapcdn.com/bootstrap/3.2.0/js/bootstrap.min.js"></script>

</head>
<body>
<div class="container">
    <div class="well">
        Write PHP script to read the data from http://courses.ics.hawaii.edu/ics215f15/morea/040.php/quiz-46em.data. The data consists of integers on their own line. Your script should calculate the minimum, maximum, and average for the data and display the answers.
    </div>
    <div class="row">
        <?php
        // your code goes here

        $content = file_get_contents("http://courses.ics.hawaii.edu/ics215f15/morea/040.php/quiz-m46e.data");
        $split = explode("\n", $content);
        echo count($split);
        echo "\n";
        $min =-1;
        $max = -1;
        $total = 0;
        for ($i = 0; $i < count($split); ++$i) {
            $val = intval($split[$i]);
            if($val > $max || $max == -1) {
                $max = $val;
            }
            if($val < $min || $min == -1) {
                $min = $val;
            }
            $total = $total + $val;
        }
        echo "Min: " . $min . " ; " . "Max: " . $max . " ; " . "Average: " . ($total / count($split)) . " ; ";
        ?>

    </div>
</div>
</body>
</html>
