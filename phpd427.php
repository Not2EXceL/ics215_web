<!DOCTYPE html>
<html>
<head>
    <title> PHP Quiz easy </title>
    <meta name="viewport" content="width=device-width, initial-scale=1.0">
    <meta charset="utf-8">

    <!-- HTML5 shim and Respond.js IE8 support of HTML5 elements and media queries -->
    <!--[if lt IE 9]>
    <script src="http://cdnjs.cloudflare.com/ajax/libs/html5shiv/3.6.2/html5shiv.js"></script>
    <script src="http://cdnjs.cloudflare.com/ajax/libs/respond.js/1.2.0/respond.js"></script>
    <![endif]-->

    <!-- Load Bootstrap JavaScript components -->
    <script src="http://code.jquery.com/jquery-2.1.1.min.js"></script>
    <!-- Latest compiled and minified CSS -->
    <link rel="stylesheet" href="https://maxcdn.bootstrapcdn.com/bootstrap/3.2.0/css/bootstrap.min.css">
    <!-- Latest compiled and minified JavaScript -->
    <script src="https://maxcdn.bootstrapcdn.com/bootstrap/3.2.0/js/bootstrap.min.js"></script>

</head>
<body>
<div class="container">
    <div class="well">
        <p>Write PHP script to check to see if the 'login' cookie is set.</p>

        <p>If it is set the script should display the login of names and a form for entering more names.</p>

        <p>If the cookie is not set the script presents creates the cookie and presents a form for entering names into
            the login cookie.</p>

        <p>You can put your PHP code anywhere.</p>
    </div>
    <div class="row">
        <p>Enter names into the 'login' cookie: </p>

        <form name="inputform" action="<?php echo htmlspecialchars($_SERVER["PHP_SELF"]); ?>" method="post">
            Name: <input type="text" name="name"><br>
            <input type="submit">
            <br>
            <button name="reset" type="submit" value="reset">Reset</button>
        </form>
        <?php
        $cookie = null;
        if ($_SERVER["REQUEST_METHOD"] == "POST") {
            if (isset($_POST["reset"])) {
                $cookie = array();
                setcookie('login', serialize($cookie));
            } else {
                $name = clean_input($_POST["name"]);
                if (strcmp($name, "") !== 0) {
                    if (array_key_exists('login', $_COOKIE)) {
                        $cookie = $_COOKIE['login'];
                        $cookie = unserialize($cookie);
                    }
                    $cookie[] = $name;
                    setcookie('login', serialize($cookie)); //expires at end of session
                }
            }
        }
        if ($cookie !== null || count($cookie) == 0) {
            if (count($cookie) > 0) {
                foreach ($cookie as $s) {
                    echo $s . "<br>";
                }
            } else {
                echo "The 'login' cookie has no data. Please submit some. <br>";
            }
        } else if (array_key_exists('login', $_COOKIE)) {
            $cookie = $_COOKIE[''];
            $cookie = unserialize($cookie);
            if (count($cookie) > 0) {
                foreach ($cookie as $s) {
                    echo $s . "<br>";
                }
            } else {
                echo "The 'login' cookie has no data. Please submit some. <br>";
            }
        } else {
            $cookie = array();
            echo "The 'login' cookie has no data. Please submit some. <br>";
        }

        function clean_input($data)
        {
            $data = trim($data);
            $data = stripslashes($data);
            $data = htmlspecialchars($data);
            return $data;
        }

        ?>
    </div>
</div>
</body>
</html>
