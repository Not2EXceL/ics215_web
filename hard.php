<!DOCTYPE html>
<html>
<head>
    <title> PHP Quiz Difficult </title>
    <meta name="viewport" content="width=device-width, initial-scale=1.0">
    <meta charset="utf-8">

    <!-- HTML5 shim and Respond.js IE8 support of HTML5 elements and media queries -->
    <!--[if lt IE 9]>
    <script src="http://cdnjs.cloudflare.com/ajax/libs/html5shiv/3.6.2/html5shiv.js"></script>
    <script src="http://cdnjs.cloudflare.com/ajax/libs/respond.js/1.2.0/respond.js"></script>
    <![endif]-->

    <!-- Load Bootstrap JavaScript components -->
    <script src="http://code.jquery.com/jquery-2.1.1.min.js"></script>
    <!-- Latest compiled and minified CSS -->
    <link rel="stylesheet" href="https://maxcdn.bootstrapcdn.com/bootstrap/3.2.0/css/bootstrap.min.css">
    <!-- Latest compiled and minified JavaScript -->
    <script src="https://maxcdn.bootstrapcdn.com/bootstrap/3.2.0/js/bootstrap.min.js"></script>

</head>
<body>
<div class="container">
    <div class="well">
        <p>Write PHP script to create a form to record a person's name and their estimated time to complete this
            assignment.</p>

        <p>The names and estimates should be persistent.</p>

        <p>Below the form show the estimates.</p>

        <p>You can put your PHP code anywhere.</p>
    </div>
    <div class="row">
        <?php

        /*
            <form action="<?php echo htmlspecialchars($_SERVER["PHP_SELF"]);?>" method="post">
              Name: <input type="text" name="name"><br>
              Estimate: <input type="number" name="estimate"><br>
              <input type="submit">
            </form>

            if($_SERVER["REQUEST_METHOD"] == "POST") {
              $name = test_input($_POST["name"]);
              $estimate = test_input($_POST["estimate"]);
              appendData($filename, $name, $estimate);
            }

            function test_input($data) {
              $data = trim($data);
              $data = stripslashes($data);
              $data = htmlspecialchars($data);
              return $data;
            }
         */

        $dom = new DOMDocument('1.0');
        $form = $dom->createElement('form');
        $form->setAttribute('method', 'post');
        $form->setAttribute('action', 'hard.php');
        $label = $dom->createElement('label', 'Name');
        $input = $dom->createElement('input');
        $input->setAttribute('name', 's_name');
        $label->appendChild($input);
        $domAttribute = $dom->createAttribute('type');
        $domAttribute->value = 'text';
        $input->appendChild($domAttribute);

        $label2 = $dom->createElement('label', 'Estimated Completion Time');
        $input2 = $dom->createElement('input');
        $input2->setAttribute('name', 'est_time');
        $label2->appendChild($input2);
        $domAttribute2 = $dom->createAttribute('type');
        $domAttribute2->value = 'text';
        $input2->appendChild($domAttribute2);

        $button = $dom->createElement('button', 'Submit');
        $button->setAttribute('name', 'submit_data');
        $clickAttr = $dom->createAttribute('submit');
        $button->appendChild($clickAttr);

        $dom->appendChild($form);
        $form->appendChild($label);
        $form->appendChild($dom->createElement('br'));
        $form->appendChild($input);

        $form->appendChild($dom->createElement('br'));

        $form->appendChild($label2);
        $form->appendChild($dom->createElement('br'));
        $form->appendChild($input2);

        $form->appendChild($dom->createElement('br'));
        $form->appendChild($dom->createElement('br'));
        $form->appendChild($button);

        print $dom->saveHTML();

        $file = "input.txt";

        if (isset($_POST['s_name']) &&
            isset($_POST['est_time'])
        ) {
            $name = $_POST['s_name'];
            $time = $_POST['est_time'];
            if($name == "" ) {
                echo "You did not input a name.";
            } else if($time == "") {
                echo "You did not input an estimated time.";
            } else {
               $input = "Name: " . $name . "; Time: " . $time . ";";
                file_put_contents($file, $input . PHP_EOL, FILE_APPEND);
            }
            unset($_POST);
        }

        // your code goes here
        $read = file_get_contents($file);
        $explode = explode("\n", $read);
        foreach ($explode as $s) {
            echo $s . "<br />";
        }
        ?>
    </div>
</div>
</body>
</html>
