<?php
//
?>
<!DOCTYPE html>
<html>
<head>
    <title> PHP Quiz easy </title>
    <meta name="viewport" content="width=device-width, initial-scale=1.0">
    <meta charset="utf-8">

    <!-- HTML5 shim and Respond.js IE8 support of HTML5 elements and media queries -->
    <!--[if lt IE 9]>
    <script src="http://cdnjs.cloudflare.com/ajax/libs/html5shiv/3.6.2/html5shiv.js"></script>
    <script src="http://cdnjs.cloudflare.com/ajax/libs/respond.js/1.2.0/respond.js"></script>
    <![endif]-->

    <!-- Load Bootstrap JavaScript components -->
    <script src="http://code.jquery.com/jquery-2.1.1.min.js"></script>
    <!-- Latest compiled and minified CSS -->
    <link rel="stylesheet" href="https://maxcdn.bootstrapcdn.com/bootstrap/3.2.0/css/bootstrap.min.css">
    <!-- Latest compiled and minified JavaScript -->
    <script src="https://maxcdn.bootstrapcdn.com/bootstrap/3.2.0/js/bootstrap.min.js"></script>

</head>
<body>
<div class="container">
    <div class="well">
        <p>Write PHP script to create a login form. The login form checks for a session cookie.</p>
        <p>If the cookie is not set the form should have a username and password input field with a login button. The form does not need to check the password, but when the user logs in, the PHP script should set up a session cookie.</p>
        <p>If the session cookie is set the login form should change to be a logout button. When the logout button is pressed the session cookie should be revoked.</p>
        <p>You can put your PHP code anywhere.</p>
    </div>
    <div class="row">
        <?php
        $dom = new DOMDocument('1.0');
        $form = $dom->createElement('form');
        $form->setAttribute('method', 'post');
        $form->setAttribute('action', 'phpm8e3.php');
        $label = $dom->createElement('label', 'File name:');
        $input = $dom->createElement('input');
        $input->setAttribute('name', 'fileName');
        $label->appendChild($input);
        $domAttribute = $dom->createAttribute('type');
        $domAttribute->value = 'text';
        $input->appendChild($domAttribute);

        $button = $dom->createElement('button', 'Get file');
        $button->setAttribute('name', 'a');
        $clickAttr = $dom->createAttribute('submit');
        $clickAttr->value = "clicked!";
        $button->appendChild($clickAttr);

        $dom->appendChild($form);
        $form->appendChild($label);
        $form->appendChild($dom->createElement('br'));
        $form->appendChild($input);
           $dom->
        $form->appendChild($dom->createElement('br'));
        $form->appendChild($dom->createElement('br'));

        ?>
    </div>
</div>
</body>
</html>
