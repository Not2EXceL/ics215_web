<!DOCTYPE html>
<html>
<head>
    <title> PHP Quiz easy </title>
    <meta name="viewport" content="width=device-width, initial-scale=1.0">
    <meta charset="utf-8">

    <!-- HTML5 shim and Respond.js IE8 support of HTML5 elements and media queries -->
    <!--[if lt IE 9]>
    <script src="http://cdnjs.cloudflare.com/ajax/libs/html5shiv/3.6.2/html5shiv.js"></script>
    <script src="http://cdnjs.cloudflare.com/ajax/libs/respond.js/1.2.0/respond.js"></script>
    <![endif]-->

    <!-- Load Bootstrap JavaScript components -->
    <script src="http://code.jquery.com/jquery-2.1.1.min.js"></script>
    <!-- Latest compiled and minified CSS -->
    <link rel="stylesheet" href="https://maxcdn.bootstrapcdn.com/bootstrap/3.2.0/css/bootstrap.min.css">
    <!-- Latest compiled and minified JavaScript -->
    <script src="https://maxcdn.bootstrapcdn.com/bootstrap/3.2.0/js/bootstrap.min.js"></script>

</head>
<body>
<div class="container">
    <div class="well">
        <p>Write PHP script to check to see if a PHP session is active.</p>

        <p>If the session is active the script should display the history of names and a form for entering more names.
            The names should be stored in the session.</p>

        <p>If the session is not started the script starts a session and presents a form for entering names into the
            session history.</p>

        <p>You can put your PHP code anywhere.</p>
    </div>
    <div class="row">
        <p>Enter names into the php session: </p>

        <form name="inputform" action="<?php echo htmlspecialchars($_SERVER["PHP_SELF"]); ?>" method="post">
            Name: <input type="text" name="name"><br>
            <input type="submit">
            <br>
            <button name="reset" type="submit" value="reset">Reset</button>
        </form>
        <?php
        $id = session_id();
        if ($id == '') {
            session_name('TSession');
            session_start();
        }

        if (count($_SESSION["names"]) < 1) {
            $_SESSION["names"] = array();
        }
        echo($_SERVER["REQUEST_METHOD"] == "POST");
        if (isset($_POST["reset"])) {
            session_destroy();
            session_name('TSession');
            session_start();
            $_SESSION['names'] = array();
        } else {
            $name = clean_input($_POST["name"]);
            if (strcmp($name, "") !== 0) {
                $decoded = session_decode($_SESSION['names']);
                print_r($decoded);
            }
        }
        $names = $_SESSION['names'];
        if (count($names) == 0) {
            echo "There are no names in the session.";
        } else {
            print_r($names);
        }


        function clean_input($data)
        {
            $data = trim($data);
            $data = stripslashes($data);
            $data = htmlspecialchars($data);
            return $data;
        }

        ?>
    </div>
</div>
</body>
</html>
