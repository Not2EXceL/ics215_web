<!DOCTYPE html>
<html>
<head>
    <title> PHP Quiz easy </title>
    <meta name="viewport" content="width=device-width, initial-scale=1.0">
    <meta charset="utf-8">

    <!-- HTML5 shim and Respond.js IE8 support of HTML5 elements and media queries -->
    <!--[if lt IE 9]>
    <script src="http://cdnjs.cloudflare.com/ajax/libs/html5shiv/3.6.2/html5shiv.js"></script>
    <script src="http://cdnjs.cloudflare.com/ajax/libs/respond.js/1.2.0/respond.js"></script>
    <![endif]-->

    <!-- Load Bootstrap JavaScript components -->
    <script src="http://code.jquery.com/jquery-2.1.1.min.js"></script>
    <!-- Latest compiled and minified CSS -->
    <link rel="stylesheet" href="https://maxcdn.bootstrapcdn.com/bootstrap/3.2.0/css/bootstrap.min.css">
    <!-- Latest compiled and minified JavaScript -->
    <script src="https://maxcdn.bootstrapcdn.com/bootstrap/3.2.0/js/bootstrap.min.js"></script>

</head>
<body>
<div class="container">
    <div class="well">
        Write PHP script to read in the data from http://courses.ics.hawaii.edu/ics215f15/morea/040.php/quiz-47bm.data. The data consists of lines with:
        <ul>
            <li>a name</li>
            <li>a comma</li>
            <li>score</li>
        </ul>
        Calculate and present the average, minimum, and maximum scores.
    </div>
    <div class="row">
        <?php
        // your code goes here
        $content = file_get_contents("http://courses.ics.hawaii.edu/ics215f15/morea/040.php/quiz-m47b.data");
        print_r($content);
        $pattern = "/(Student\d+\,\d+[\s]?)\$/";
        preg_match_all ($pattern, $content, $matches);

        echo "<br />";
        $i = 0;
        print_r($matches);
        echo "<br />";
        print_r(count($matches));
        ?>
    </div>
</div>
</body>
</html>
