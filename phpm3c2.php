<!DOCTYPE html>
<html>
<head>
    <title> PHP Quiz easy </title>
    <meta name="viewport" content="width=device-width, initial-scale=1.0">
    <meta charset="utf-8">

    <!-- HTML5 shim and Respond.js IE8 support of HTML5 elements and media queries -->
    <!--[if lt IE 9]>
    <script src="http://cdnjs.cloudflare.com/ajax/libs/html5shiv/3.6.2/html5shiv.js"></script>
    <script src="http://cdnjs.cloudflare.com/ajax/libs/respond.js/1.2.0/respond.js"></script>
    <![endif]-->

    <!-- Load Bootstrap JavaScript components -->
    <script src="http://code.jquery.com/jquery-2.1.1.min.js"></script>
    <!-- Latest compiled and minified CSS -->
    <link rel="stylesheet" href="https://maxcdn.bootstrapcdn.com/bootstrap/3.2.0/css/bootstrap.min.css">
    <!-- Latest compiled and minified JavaScript -->
    <script src="https://maxcdn.bootstrapcdn.com/bootstrap/3.2.0/js/bootstrap.min.js"></script>

</head>
<body>
<div class="container">
    <div class="well">
        <p>Write PHP script to create an HTML form that asks for a user name and student id. The PHP script should
            validate the form information, specifically the student id when the submit button is pressed.</p>

        <p>Valid student ids have four digits, an optional space or dash, four more digits.</p>
    </div>
    <div class="row">
        <?php
        // your code goes here

        $dom = new DOMDocument('1.0');
        $form = $dom->createElement('form');
        $form->setAttribute('method', 'post');
        $form->setAttribute('action', 'phpm3c2.php');
        $label = $dom->createElement('label', 'Name');
        $input = $dom->createElement('input');
        $label->appendChild($input);
        $domAttribute = $dom->createAttribute('type');
        $domAttribute->value = 'text';
        $input->appendChild($domAttribute);
        $domAttribute = $dom->createAttribute('type');
        $domAttribute->value = 'text';
        $input->appendChild($domAttribute);

        $label2 = $dom->createElement('label', 'ID Number');
        $input2 = $dom->createElement('input');
        $input2->setAttribute('name', 'id');
        $label2->appendChild($input2);
        $domAttribute2 = $dom->createAttribute('type');
        $domAttribute2->value = 'text';
        $input2->appendChild($domAttribute2);
        $domAttribute2 = $dom->createAttribute('type');
        $domAttribute2->value = 'text';
        $input2->appendChild($domAttribute2);

        $button = $dom->createElement('button', 'Validate ID');
        $button->setAttribute('name', 'a');
        $clickAttr = $dom->createAttribute('submit');
        $clickAttr->value = "clicked!";
        $button->appendChild($clickAttr);

        $dom->appendChild($form);
        $form->appendChild($label);
        $form->appendChild($dom->createElement('br'));
        $form->appendChild($input);

        $form->appendChild($dom->createElement('br'));

        $form->appendChild($label2);
        $form->appendChild($dom->createElement('br'));
        $form->appendChild($input2);

        $form->appendChild($dom->createElement('br'));
        $form->appendChild($dom->createElement('br'));
        $form->appendChild($button);

        print $dom->saveHTML();

        if (isset($_POST['id'])) {
            $id = $_POST['id'];
            if($id == "") {
                 echo "You didn't input an id.";
            } elseif (!preg_match("/^\d{4}[\s-]?\d{4}$/", $id)) {
                echo "'" . $id . "' is not a valid id number";
            } else {
                echo "'" . $id . "' valid";
            }
            $_POST = array();
        }

        ?>
    </div>
</div>
</body>
</html>
