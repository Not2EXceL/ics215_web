<!DOCTYPE html>
<html>
<head>
    <title> PHP Quiz easy </title>
    <meta name="viewport" content="width=device-width, initial-scale=1.0">
    <meta charset="utf-8">

    <!-- HTML5 shim and Respond.js IE8 support of HTML5 elements and media queries -->
    <!--[if lt IE 9]>
    <script src="http://cdnjs.cloudflare.com/ajax/libs/html5shiv/3.6.2/html5shiv.js"></script>
    <script src="http://cdnjs.cloudflare.com/ajax/libs/respond.js/1.2.0/respond.js"></script>
    <![endif]-->

    <!-- Load Bootstrap JavaScript components -->
    <script src="http://code.jquery.com/jquery-2.1.1.min.js"></script>
    <!-- Latest compiled and minified CSS -->
    <link rel="stylesheet" href="https://maxcdn.bootstrapcdn.com/bootstrap/3.2.0/css/bootstrap.min.css">
    <!-- Latest compiled and minified JavaScript -->
    <script src="https://maxcdn.bootstrapcdn.com/bootstrap/3.2.0/js/bootstrap.min.js"></script>

</head>
<body>
<div class="container">
    <div class="well">
        Write PHP scripts to create a login/registration page.
        <p>The page should have a form that asks for a user name and password. The form should have a register button
            that changes the form to have user name, password, and confirm password (This could be another PHP page).
            When the user registers the PHP script should validate the password ensuring that it has at least one digit,
            one lowercase letter and one uppercase letter. If the passwords match and are valid, the script should save
            the username and password in a text file on the server.</p>

        <p>If the user hits the login button the PHP script should check the username and password against the text file
            and indicate if they entered a valid username and password. If the user didn't enter a valid username or
            password show an error message.</p>

        <p>You can put your PHP code anywhere.</p>
    </div>
    <div class="row">
        <?php
        $user_key = "user";
        $register_key = "register";
        $login_key = "login";
        $file = "login_info.data";
        if (!file_exists($file)) {
            $handle = fopen($file, "x");
            fclose($handle);
        }
        $pattern = "/^(?=.*\d)(?=.*[A-Z])(?=.*[a-z])\S*$/";

        function session_setup($key)
        {
            global $user_key, $register_key, $login_key;
            if (session_status() !== PHP_SESSION_ACTIVE) session_start();
            $_SESSION[$user_key] = $key === $user_key;
            $_SESSION[$register_key] = $key === $register_key;
            $_SESSION[$login_key] = $key === $login_key;
        }

        session_setup($user_key);

        if ($_SERVER["REQUEST_METHOD"] == "POST") {
            if (post_exists("user")) {
                session_setup("user");
                if (!post_exists("username")) {
                    echo "You failed to input a username.<br>";
                } else {
                    $read = file_get_contents($file);
                    $explode = explode("\n", $read);
                    $user_exists = false;
                    foreach ($explode as $s) {
                        if (startsWith($s, post_clean("username") . ":")) {
                            $user_exists = true;
                            break;
                        }
                    }
                    if (!$user_exists) {
                        echo "Register<br>";
                        session_setup("register");
                    } else {
                        session_setup("login");
                    }
                }
            } else if (post_exists("back")) {
                session_setup("user");
            } else if (post_exists("register")) {
                session_setup($register_key);
                $arr = array("username", "password", "password2");
                $all = all_post_exists($arr);
                $missing = false;
                foreach ($all as $k => $b) {
                    if (!$b) {
                        if ($k === "password2") {
                            echo "You failed to retype the password." . "<br>";
                        } else {
                            echo "You failed to input a " . $k . ".<br>";
                        }
                        $missing = true;
                        continue;
                    }
                }
                echo "<br>";
                if (!$missing) {
                    $get = post_data_all($arr);
                    $u = $get["username"]["data"];
                    $p = $get["password"]["data"];
                    $p_two = $get["password2"]["data"];
                    if ($p !== $p_two) {
                        echo "You failed to retype the password.";
                    } else {
                        preg_match($pattern, $p, $p_match);
                        preg_match($pattern, $p_two, $p_match_two);
                        if (count($p_match) >= 1 && count($p_match_two) >= 1) {
                            $input = $u . ":" . $p . "\n";
                            file_put_contents($file, $input . PHP_EOL, FILE_APPEND);
                            echo "Registered Username: " . $u . " | Password: " . $p . "<br>";
                            session_setup($login_key);
                        } else {
                            if (!$p_match) echo "The initial password must have 1 Digit, 1 Uppercase and 1 Lowercase.<br>";
                            if (!$p_match) echo "The re-typed password must have 1 Digit, 1 Uppercase and 1 Lowercase.<br>";
                        }
                    }
                }
            } else if (post_exists("login")) {
                session_setup($login_key);
                $arr = array("username", "password");
                $read = file_get_contents($file);
                $explode = explode("\n", $read);
                $user_exists = false;
                $success = false;
                foreach ($explode as $s) {
                    if ($s === "") continue;
                    $all = all_post_exists($arr);
                    $missing = false;
                    foreach ($all as $k => $b) {
                        if (!$b) {
                            echo "You failed to input a " . $k . ".<br>";
                            $missing = true;
                            continue;
                        }
                    }
                    echo "<br>";
                    if (!$missing) {
                        $get = post_data_all($arr);
                        $u = $get["username"]["data"];
                        $p = $get["password"]["data"];
                        $join = $u . ":" . $p;
                        if ($s === $join) {
                            echo "You successfully logged in.";
                            session_setup($user_key);
                            $success = true;
                            break;
                        }
                    }
                }
                if (!$success) echo "Username and Password don't match stored data.";
            }
        }

        if (is_session($user_key)) {
            ?>
            <form action="<?php echo htmlspecialchars($_SERVER["PHP_SELF"]); ?>" method="post">
                Username: <input type="text" name="username" value=<?php echo post_clean("username") ?>><br>
                <button name="user" type="submit" value="click" style="margin: 10px 10px 10px;">User</button>
            </form>
            <?php
        } else if (is_session($register_key)) {
            ?>
            <form action="<?php echo htmlspecialchars($_SERVER["PHP_SELF"]); ?>" method="post">
                Username: <input type="text" name="username" value=<?php echo post_clean("username") ?>><br>
                Password: <input type="text" name="password" value=<?php echo post_clean("password") ?>><br>
                Re-type: <input type="text" name="password2" value=<?php echo post_clean("password2") ?>><br>
                <button name="register" type="submit" value="click" style="margin: 10px 10px 10px;">Register</button>
            </form>
            <?php
        } else if (is_session($login_key)) {
            ?>
            <form action="<?php echo htmlspecialchars($_SERVER["PHP_SELF"]); ?>" method="post">
                Username: <input type="text" name="username" value=<?php echo post_clean("username") ?>><br>
                Password: <input type="text" name="password" value=<?php echo post_clean("password") ?>><br>
                <button name="login" type="submit" value="click" style="margin: 10px 10px 10px;">Login</button>
                <button name="back" type="submit" value="click" style="margin: 10px 10px 10px;">Back</button>
            </form>
            <?php
        }

        function startsWith($s1, $s2)
        {
            return $s2 === "" || strrpos($s1, $s2, -strlen($s1)) !== FALSE;
        }

        function post_data_all($data)
        {
            $array = array();
            foreach ($data as $d) {
                $array[$d] = post_data($d);
            }
            return $array;
        }

        function post_data($data)
        {
            $exists = post_exists($data);
            if ($exists) $data = post_clean($data);
            $arr = array("exists" => $exists, "data" => $data);
            return $arr;
        }

        function post_exists($data)
        {
            if (isset($_POST[$data]) && $_POST[$data] !== "")
                return true;
            return false;
        }

        function is_session($data)
        {
            if (isset($_SESSION[$data]) && $_SESSION[$data] !== false)
                return true;
            return false;
        }

        function all_post_exists($data)
        {
            $array = array();
            foreach ($data as $d) {
                $array[$d] = post_exists($d);
            }
            return $array;
        }

        function post_clean($data)
        {
            if (!post_exists($data)) return "";
            $data = $_POST[$data];
            $data = trim($data);
            $data = stripslashes($data);
            $data = htmlspecialchars($data);
            return $data;
        }

        ?>
    </div>
</div>
</body>
</html>
