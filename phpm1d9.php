<!DOCTYPE html>
<html>
<head>
    <title> PHP Quiz easy </title>
    <meta name="viewport" content="width=device-width, initial-scale=1.0">
    <meta charset="utf-8">

    <!-- HTML5 shim and Respond.js IE8 support of HTML5 elements and media queries -->
    <!--[if lt IE 9]>
    <script src="http://cdnjs.cloudflare.com/ajax/libs/html5shiv/3.6.2/html5shiv.js"></script>
    <script src="http://cdnjs.cloudflare.com/ajax/libs/respond.js/1.2.0/respond.js"></script>
    <![endif]-->

    <!-- Load Bootstrap JavaScript components -->
    <script src="http://code.jquery.com/jquery-2.1.1.min.js"></script>
    <!-- Latest compiled and minified CSS -->
    <link rel="stylesheet" href="https://maxcdn.bootstrapcdn.com/bootstrap/3.2.0/css/bootstrap.min.css">
    <!-- Latest compiled and minified JavaScript -->
    <script src="https://maxcdn.bootstrapcdn.com/bootstrap/3.2.0/js/bootstrap.min.js"></script>

</head>
<body>
<div class="container">
    <div class="well">
        <p>Write PHP script to add a custom error handler to your PHP page. The error handler should display the Error
            Level, Error Message, Filename and line number where the error occurs.</p>

        <p>You can put your PHP code anywhere.</p>
    </div>
    <div class="row">
        <?php
        //
//        function startsWith($source)
//        {
//            $prefix = "E";
//            return strncmp(key($source), $prefix, strlen($prefix)) == 0;
//        }

        function custom($num, $str, $file, $line)
        {
            $consts = get_defined_constants('Core_d');
            $newArr = array_filter($consts, "startsWith");
        }

        set_error_handler('custom');
        trigger_error("Fake Error", E_USER_WARNING);
        ?>
    </div>
</div>
</body>
</html>
