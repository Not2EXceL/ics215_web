import os

def main():
    min = None
    max = None
    total = 0
    count = 0
    s = raw_input("Enter file name (Nothing for default m631.txt): ")
    if s == "":
        s = "./m631.txt"
    lines = [line.rstrip('\n') for line in open(s)]
    for line in lines:
        text = line.split(", ")
        if min == None:
            min = text
        if int(min[1]) > int(text[1]):
            min = text
        if max == None:
            max = text
        if int(max[1]) < int(text[1]):
            max = text
        total += int(text[1])
        count += 1
    print "Mininmum: Name: " + min[0] + "; Score: " + min[1]
    print "Maximum: Name: " + max[0] + "; Score: " + max[1]
    print "Average Score: " + str(float(total) / float(count))

if __name__ == "__main__":
    main()
