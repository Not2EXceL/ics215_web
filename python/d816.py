import urllib
import xml.etree.ElementTree as ET


def main():
    tags = {"temp_f": "Temperature: <data> F",
            "relative_humidity": "Humidity: <data> %",
            "wind_mph": "Wind Speed: <data> mph"}
    weather_data = []
    socket = urllib.urlopen("http://w1.weather.gov/xml/current_obs/PHNL.xml")
    html = socket.read()
    socket.close()
    root = ET.fromstring(html)
    for child in root:
        if child.tag not in tags:
            continue
        weather_data.append(tags[child.tag].replace("<data>", child.text))
    print "Weather Data"
    print "============"
    for s in weather_data:
        print s


if __name__ == "__main__":
    main()
# end if
