def main():
    scores = []
    s = raw_input("Enter file name (Nothing for default mdc5.txt): ")
    if s == "":
        s = "./mdc5.txt"
    lines = [line.rstrip('\n') for line in open(s)]
    for line in lines:
        text = line.split(", ")
        scores.append((text[0], int(text[1])))
    sorted_scores = sorted(scores, key=lambda x: x[1], reverse=True)
    print '<table border="1">'
    print '  <tr>'
    print '    <td>Name</td>'
    print '    <td>Score</td>'
    for score in sorted_scores:
        print '  <tr>'
        print '    <td>' + score[0] + '</td>'
        print '    <td>' + str(score[1]) + '</td>'
        print '  </tr>'
    print '</table>'

if __name__ == "__main__":
    main()
