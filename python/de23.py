import urllib
import xml.etree.ElementTree as ET


def main():
    file_name = 'de23.txt'
    temp_data = []
    all_valid = True
    saved = open(file_name, 'a+')
    for t in saved.readlines():
        try:
            check = float(t)
            temp_data.append(check)
        except ValueError:
            all_valid = False
            pass
    print "Save data line count: " + str(len(temp_data))
    if not all_valid:
        print "Failed to load all save data."
        print "Removing corrupt data.\n"
        saved.seek(0)
        saved.truncate()
        for data in temp_data:
            saved.write(str(data))
            saved.write("\n")
    else:
        print "Save data loaded and validated."

    temp_tag = "temp_f"
    socket = urllib.urlopen("http://w1.weather.gov/xml/current_obs/PHNL.xml")
    html = socket.read()
    socket.close()
    root = ET.fromstring(html)
    for child in root:
        if child.tag == temp_tag:
            value = child.text
            print "Current Temperature: " + value + " F"
            temp_data.append(value)
            saved.write(value)
            saved.write("\n")
    total = float(0)
    for temp in temp_data:
        total += float(temp)
    average = total / len(temp_data)
    print "Average Temperature: " + str(average) + " F"
    saved.close()


if __name__ == "__main__":
    main()
# end if
