import os
import fnmatch

def main():
    matched_files = []
    s = raw_input("Enter regex: ")
    files = os.listdir('.')
    for f in files:
        if os.path.isdir(f):
            continue
        if fnmatch.fnmatch(f, s):
            matched_files.append(f)
    if not matched_files:
        print "Found no files matching regex {}".format(s)
    else:
        print "Matched Files: "
        for m in matched_files:
            print "  {}".format(m)

if __name__ == "__main__":
    main()
