import os


def main():
    filecount = {}
    dirpath = raw_input('Enter a directory: ')
    if dirpath == '':
        dirpath = '.'
    files = os.listdir(dirpath)
    for f in files:
        file = os.path.join(dirpath, f)
        if os.path.isdir(file):
            i = filecount.get('dir')
            if not i:
                filecount['dir'] = 1
            else:
                filecount['dir'] = i + 1
        else:
            name, ext = os.path.splitext(file)
            i = filecount.get(ext)
            if not i:
                filecount[ext] = 1
            else:
                filecount[ext] = i + 1
    for ext in filecount:
        if ext == 'dir':
            print "Directory Count: {}".format(filecount[ext])
        else:
            print "Extension: {}; Count: {}".format(ext, filecount[ext])


if __name__ == "__main__":
    main()
