import os

def main():
    extensions = {}
    # s = raw_input("Enter a directory: (default is '.')")
    s = '.'
    for f in os.listdir(s):
        fn = os.path.join(s, f)
        if os.path.isdir(fn):
            i = extensions.get('dir')
            if not i:
                extensions['dir'] = 1
            else:
                extensions['dir'] = i + 1
        else:
            name, ext = os.path.splitext(f)
            i = extensions.get(ext)
            if not i:
                extensions[ext] = 1
            else:
                extensions[ext] = i + 1
    for ext in extensions:
        print "Extension: {}; Count: {}".format(ext, extensions[ext])

if __name__ == '__main__':
    main()
