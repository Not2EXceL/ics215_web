import os
from datetime import datetime

date_format = "%m/%d/%y"

def main():
    # ex_date = '01/27/15 01:56:58'
    date1 = None
    date2 = None

    while not date1:
        try:
            s = raw_input("Enter 1st date: (format => MM/DD/YY): ")
            if s.lower() == "q" or s.lower() == "quit":
                print "Quitting"
                return
            else:
                date1 = datetime.strptime(s, date_format)
        except:
            pass
    while not date2:
        try:
            s = raw_input("Enter 2nd date: (format => MM/DD/YY): ")
            if s.lower() == "q" or s.lower() == "quit":
                print "Quitting"
                return
            else:
                date2 = datetime.strptime(s, date_format)
        except:
            pass
    if date1 > date2 :
        print '2nd Date is before the 1st Date. Swapping dates.'
        temp = date1
        date1 = date2
        date2 = temp
    print '1st Date: {}'.format(date1.date())
    print '2nd Date: {}'.format(date2.date())
    print '{} days between the two dates'.format((date2 - date1).days)
if __name__ == "__main__":
    main()
    
