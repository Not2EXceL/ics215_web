import os
import sys
from datetime import datetime

time_format = '%m/%d/%y %H:%M:%S'


def main():
    valid_files = []

    # ex_time = '01/27/15 01:56:58'
    itime = None

    pass
    while True:
        try:
            s = raw_input("Enter time (format => MM/DD/YY hh:mm:ss): ")
            if s.lower() == "q" or s.lower() == "quit":
                print "Quitting."
                return
            else:
                itime = datetime.strptime(s, time_format)
                break
	    # end if
        except:
            pass
	# end try
    # end while

    files = os.listdir('.')
    for f in files:
        if os.path.isdir(f):
            continue
        ctime = datetime.fromtimestamp(os.stat(f).st_ctime)
        if ctime >= itime:
            valid_files.append(f)
	# end if
    # end for
    title = "Files created after " + itime.ctime()
    breaker = ""
    for _ in range(len(title)):
        breaker += "="
    # end for
    print title
    print breaker
    for f in valid_files:
        print f
    # end for


# end def main


if __name__ == "__main__":
    main()
# end if
