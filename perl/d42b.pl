#!/usr/bin/perl
use strict;
use warnings FATAL => 'all';

use LWP::Simple;
use XML::Simple;

#2. Write a Perl script that gets the current temperature data from http://w1.weather.gov/xml/current_obs/PHNL.xml.
#    The script should load the current weather XML data from http://w1.weather.gov/xml/current_obs/PHNL.xml.
#
#    It should parse the xml and report the current temperature and append the temperature in a text file named 42bd.txt. Make sure the file is saved.
#
#    The script should calculate and output the average temperature based upon the stored data.

my $file = '42bd.txt';
my @temps;

open(FILE, "+>>", $file);
@temps = <FILE>;

my $url = 'http://w1.weather.gov/xml/current_obs/PHNL.xml';
my $content = get($url);

my $xml = XML::Simple->new;
my $data = $xml->XMLin($content);
my $temp = $data->{'temp_f'};
push @temps, $temp;
print FILE $temp . "\n";
close(FILE);

my $total = 0;
my $count = 0;

foreach my $t(@temps) {
    $total += $t;
    $count += 1;
}

my $average = $total / $count;

print "Current Temp: " . $temp . "\n";
print "Average Temp: " . $average . "\n";

exit(0);
