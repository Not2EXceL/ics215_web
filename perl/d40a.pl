#!/usr/bin/perl
use strict;
use warnings FATAL => 'all';

use LWP::Simple;
use XML::Simple;

#2. Write a Perl script that loads an HTML table from http://courses.ics.hawaii.edu/ics215f15/morea/050.perl/quiz-d40a.data.
#    The table is a two column table consisting of Names and Scores.
#
#    The script should strip out the names and scores, calculate the minimum, average, and maximum scores.
#    The script should output the minimum and maximum player score with the player’s names.
#    The script should also output the average player score.

my $url = 'http://courses.ics.hawaii.edu/ics215f15/morea/050.perl/quiz-d40a.data';
my $content = get($url);

my $xml = XML::Simple->new;

my $data = $xml->XMLin($content);
my $min = -1;
my $minName = "";
my $max = -1;
my $maxName = "";
my $total = 0;
my $count = 0;

for my $tr (@{ $data->{tbody}->{tr} }) {
    my $td = $tr->{'td'};
    my $student = $td->[0];
    my $score = $td->[1];
    if($min == -1 || $min > $score) {
        $min = $score;
        $minName = $student;
    }
    if($max == -1 || $score > $max) {
        $max = $score;
        $maxName = $student;
    }
    $total += $score;
    $count += 1;
}

my $average = $total / $count;

print "Minimum | Name: " . $minName . "; Score: " . $min . "\n";
print "Maximum | Name: " . $maxName . "; Score: " . $max . "\n";
print "Average Score: " . $average . "\n";

exit 0;
