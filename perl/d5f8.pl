#!/usr/bin/perl
use strict;
use warnings FATAL => 'all';

use LWP::Simple;
use XML::Simple;

#2. Write a Perl script that gets the current weather observations from http://w1.weather.gov/xml/current_obs/PHNL.xml.
#    The script should load the current weather XML data from http://w1.weather.gov/xml/current_obs/PHNL.xml.
#
#    It should parse the xml and report the current temperature, relative humidity, and wind speed.

my $url = 'http://w1.weather.gov/xml/current_obs/PHNL.xml';
my $content = get($url);

my $xml = XML::Simple->new;
my $data = $xml->XMLin($content);

my $temp = $data->{'temp_f'};
my $wind = $data->{'wind_mph'};
my $humid = $data->{'relative_humidity'};

print "Weather" . "\n";
print "=======" . "\n";
print "Current Temp: " . $temp . "\n";
print "Wind Speed: " . $wind . "\n";
print "Relative Humidity: " . $humid . "\n";

exit(0);
