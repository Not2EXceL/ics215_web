#!/usr/bin/perl
use strict;
use warnings FATAL => 'all';

use DateTime::Format::Strptime;

#2. Write a Perl script that list all the files in the current directory that have been modified since a given date.
#    Validate the date format. The date should have the format “MM/DD/YY hh:mm:ss”
#
#Handle issues with the date format by asking for a new date.

my $dir = ".";
my $strp = DateTime::Format::Strptime->new(
    pattern   => '%D %T',
    locale    => 'en_US'
);

my $datetime;

while(!$datetime) {
    #11/08/15 00:00:00
    print "Please input a date (MM/DD/YY hh:mm:ss): ";
    my $input = <STDIN>;
    $datetime = $strp->parse_datetime($input);
}

opendir (DIR, $dir) or die $!;

while (my $file = readdir(DIR)) {

    next if ($file =~ m/^\./);

    #($dev,$ino,$mode,$nlink,$uid,$gid,$rdev,$size,
    #$atime,$mtime,$ctime,$blksize,$blocks)

    my $fc = (stat($file))[9];
    my $diff = $fc - $datetime->epoch();
    if($diff > 0) {
        print $file . "\n";
    }
}

closedir(DIR);
exit 0;
