<!DOCTYPE html>
<html>
<head>
    <title> PHP Quiz easy </title>
    <meta name="viewport" content="winputth=device-winputth, initial-scale=1.0">
    <meta charset="utf-8">

    <!-- HTML5 shim and Respond.js IE8 support of HTML5 elements and media queries -->
    <!--[if lt IE 9]>
    <script src="http://cdnjs.cloudflare.com/ajax/libs/html5shiv/3.6.2/html5shiv.js"></script>
    <script src="http://cdnjs.cloudflare.com/ajax/libs/respond.js/1.2.0/respond.js"></script>
    <![endif]-->

    <!-- Load Bootstrap JavaScript components -->
    <script src="http://code.jquery.com/jquery-2.1.1.min.js"></script>
    <!-- Latest compiled and minified CSS -->
    <link rel="stylesheet" href="https://maxcdn.bootstrapcdn.com/bootstrap/3.2.0/css/bootstrap.min.css">
    <!-- Latest compiled and minified JavaScript -->
    <script src="https://maxcdn.bootstrapcdn.com/bootstrap/3.2.0/js/bootstrap.min.js"></script>

</head>
<body>
<div class="container">
    <div class="well">
        <p>Write PHP script to show the text files on the server and create a form asking for a data file.</p>

        <p>When the user enters the filename and it matches a text file the PHP script should read in the contents of
            the file and echo them to the page below the form.</p>
    </div>
    <div class="row">
        <?php
        // your code goes here
        $dom = new DOMDocument('1.0');
        $form = $dom->createElement('form');
        $form->setAttribute('method', 'post');
        $form->setAttribute('action', 'phpm8e3.php');
        $label = $dom->createElement('label', 'File name:');
        $input = $dom->createElement('input');
        $input->setAttribute('name', 'fileName');
        $label->appendChild($input);
        $domAttribute = $dom->createAttribute('type');
        $domAttribute->value = 'text';
        $input->appendChild($domAttribute);

        $button = $dom->createElement('button', 'Get file');
        $button->setAttribute('name', 'a');
        $clickAttr = $dom->createAttribute('submit');
        $clickAttr->value = "clicked!";
        $button->appendChild($clickAttr);

        $dom->appendChild($form);
        $form->appendChild($label);
        $form->appendChild($dom->createElement('br'));
        $form->appendChild($input);

        $form->appendChild($dom->createElement('br'));
        $form->appendChild($dom->createElement('br'));
        $form->appendChild($button);

        print $dom->saveHTML();

        if (isset($_POST['fileName'])) {
            $input = $_POST['fileName'];
            if ($input == "") {
                echo "You dinputn't input an input.";
            } else {
                foreach (glob("*.txt") as $filename) {
                    $sub = substr($filename, 0, strlen($input));
                    if ($sub != $input) {
                        continue;
                    }
                    $content = file_get_contents($filename);
                    echo $content . "<be />";
                }
            }
            $_POST = array();
        }


        ?>
    </div>
</div>
</body>
</html>
