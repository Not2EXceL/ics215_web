<!DOCTYPE html>
<html>
<head>
    <title> PHP Quiz Medium </title>
    <meta name="viewport" content="width=device-width, initial-scale=1.0">
    <meta charset="utf-8">

    <!-- HTML5 shim and Respond.js IE8 support of HTML5 elements and media queries -->
    <!--[if lt IE 9]>
    <script src="http://cdnjs.cloudflare.com/ajax/libs/html5shiv/3.6.2/html5shiv.js"></script>
    <script src="http://cdnjs.cloudflare.com/ajax/libs/respond.js/1.2.0/respond.js"></script>
    <![endif]-->

    <!-- Load Bootstrap JavaScript components -->
    <script src="http://code.jquery.com/jquery-2.1.1.min.js"></script>
    <!-- Latest compiled and minified CSS -->
    <link rel="stylesheet" href="https://maxcdn.bootstrapcdn.com/bootstrap/3.2.0/css/bootstrap.min.css">
    <!-- Latest compiled and minified JavaScript -->
    <script src="https://maxcdn.bootstrapcdn.com/bootstrap/3.2.0/js/bootstrap.min.js"></script>

</head>
<body>
<div class="container">
    <div class="well">
        Write PHP script to read in the data from http://courses.ics.hawaii.edu/ics215f15/morea/040.php/quiz-medi.data. The data consists of lines with phone numbers. Validate the phone numbers and display the valid phone numbers. Phone numbers have:
        <ul>
            <li>An optional country code, one digit,</li>
            <li>A space or dash,</li>
            <li>An optional area code, three digits with optional parentheses,</li>
            <li>A space or dash,</li>
            <li>A three digit exchange number,</li>
            <li>A space or dach,</li>
            <li>A four digit subscriber number.</li>
        </ul>
    </div>
    <div class="row">
        <?php
        // your code goes here
        $file = file('http://courses.ics.hawaii.edu/ics215f15/morea/040.php/quiz-medi.data');
        $pattern = '/\d?[\s-]?(\(?\d{3}\)?)?[\s-]?\d{3}[\s-]?\d{4}/';
        foreach ($file as $index => $line) {
//            echo $index . " -> " . $line . "<br />";
            if(preg_match($pattern, $line)) {
                echo $line . "<br />";
            }// else {
//                echo "--> " . $line . "<br />";
//            }
        }
        ?>
    </div>
</div>
</body>
</html>
