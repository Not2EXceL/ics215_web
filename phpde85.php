<!DOCTYPE html>
<html>
<head>
    <title> PHP Quiz easy </title>
    <meta name="viewport" content="width=device-width, initial-scale=1.0">
    <meta charset="utf-8">

    <!-- HTML5 shim and Respond.js IE8 support of HTML5 elements and media queries -->
    <!--[if lt IE 9]>
    <script src="http://cdnjs.cloudflare.com/ajax/libs/html5shiv/3.6.2/html5shiv.js"></script>
    <script src="http://cdnjs.cloudflare.com/ajax/libs/respond.js/1.2.0/respond.js"></script>
    <![endif]-->

    <!-- Load Bootstrap JavaScript components -->
    <script src="http://code.jquery.com/jquery-2.1.1.min.js"></script>
    <!-- Latest compiled and minified CSS -->
    <link rel="stylesheet" href="https://maxcdn.bootstrapcdn.com/bootstrap/3.2.0/css/bootstrap.min.css">
    <!-- Latest compiled and minified JavaScript -->
    <script src="https://maxcdn.bootstrapcdn.com/bootstrap/3.2.0/js/bootstrap.min.js"></script>

</head>
<body>
<div class="container">
    <div class="well">
        <p>Write PHP script to create a form and keep track of yes or no votes.</p>

        <p>The script should present the current results and allow the user to vote yes or no.</p>

        <p>The results should be persistent.</p>

        <p>You can put your PHP code anywhere.</p>
    </div>
    <div class="row">
        <p>Vote yes or no:</p>

        <form action="<?php echo htmlspecialchars($_SERVER["PHP_SELF"]); ?>" method="post">
            <button name="vote" type="submit" value="yes">Yes</button>
            <br>
            <button name="vote" type="submit" value="no">No</button>
        </form>
        <?php

        $file = "votes.txt";
        $votes = "votes";
        $yes_key = "yes";
        $no_key = "no";
        $yes = 0;
        $no = 0;

        if (!file_exists($file)) {
            $json[$votes] = array($yes_key => 0, $no_key => 0);
            file_put_contents($file, json_encode($json));
        }

        $json = json_decode(file_get_contents($file), true);
        $yes = $json[$votes][$yes_key];
        $no= $json[$votes][$no_key];

        if ($_SERVER["REQUEST_METHOD"] == "POST") {
            $name = $_POST["vote"];
            if (strcmp($name, $yes_key) === 0) {
                //yes
                $yes += 1;
            } else if (strcmp($name, $no_key) === 0) {
                //no
                $no += 1;
            }
        }
        $json[$votes] = array($yes_key => $yes, $no_key => $no);
        file_put_contents($file, json_encode($json));

        echo "Votes Yes: " . $json[$votes][$yes_key] . "<br>" .
            "Votes No: " . $json[$votes][$no_key];
        ?>
    </div>
</div>
</body>
</html>
